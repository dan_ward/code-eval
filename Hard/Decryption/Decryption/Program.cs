﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Decryption
{
    class Program
    {
        static void Main(string[] args)
        {
            using (StreamReader reader = File.OpenText("message.txt"))
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    if (null == line)
                        continue;

                    string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                    string keyed_alphabet = "BHISOECRTMGWYVALUZDNFJKPQX";

                    for(int i = 0; i < line.Length; i+=2)
                    {
                        if(line[i] == ' ')
                        {
                            Console.Write(" ");
                            i--;
                            continue;
                        }

                        int message_val = Convert.ToInt32(line.Substring(i, 2));

                        char aChar = alphabet[message_val];

                        int offset = keyed_alphabet.IndexOf(aChar);

                        char finalChar = alphabet[offset];

                        Console.Write(finalChar);
                    }



                    Console.Read();
                }
        }
    }
}
