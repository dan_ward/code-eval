﻿using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Globalization;
using System.Threading;

class Program
{
    static void Main(string[] args)
    {
        using (StreamReader reader = File.OpenText("lines.txt"))
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (null == line)
                    continue;

                var letter = "";
                for(int i = 0; i < line.Length; i++)
                {
                    letter = line[i].ToString();
                    if (i == 0)
                        letter = letter.ToUpper();
                    else if (line[i - 1] == ' ')
                        letter = letter.ToUpper();

                    Console.Write(letter);
                }
                Console.WriteLine();

            }
    }
}