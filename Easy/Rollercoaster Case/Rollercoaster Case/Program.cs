﻿using System.IO;
using System.Collections.Generic;
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        using (StreamReader reader = File.OpenText("lines.txt"))
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                if (null == line)
                    continue;

                var letter = "";

                int k = 0;
                for(int i = 0; i < line.Length; i++)
                {
                    if (Char.IsLetter(line[i]))
                    {
                        letter = k % 2 == 0 ? line[i].ToString().ToUpper() : line[i].ToString().ToLower();
                        Console.Write(letter);
                        k++;
                    }
                    else
                    {
                        Console.Write(line[i]);
                    }
                }

                Console.WriteLine();
            }
    }
}